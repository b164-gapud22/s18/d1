console.log('hello world')

// Objects

// creating objects using object initializer/literal notation
// {} => Object Literals


let cellphone = {
	name: 'Nokia 3310',
	manufacturDate: 1999
}

console.log(cellphone);
console.log(typeof cellphone);

console.log(cellphone.name)


let cellphone2 = {
	name: 'Real me super zoom',
	manufacturDate: 2019
}

let cellphone3 = {
	name: 'Samsung A7',
	manufacturDate: 2017
}

// Creating objects using a constructor function
// creates a reusable function to create several objects that have the same data structure

/*
Syntax:
	function objectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB;
	}

*/

function laptop(name, os, price){
	this.name = name;
	this.os = os;
	this.price = price;
}

// This is a unique instance of the laptop object
// The 'new' operator creates an instance of an object
// object literals
	/*
		let object = {
			key: value

		}

	*/
// Instances
	/*
		let object = new object

	*/
let laptop1 = new laptop('Lenovo', 'windows 10', 30000);
console.log(laptop1);

// This is another unique instance of the laptio object
let myLaptop = new laptop('MacBook pro', 'Catalina', 50000);

console.log(myLaptop)
// The example above invokes/calls the "laptop" function instead of creating a new object instance
// Returns "undefined" w/o operator because the "laptop" does not have a return statement

// Creating empty objects
let computer = {};
// let myComputer = new object();

// Accessing Object Properties
// using the dot notation
console.log(laptop1.name);

// using the square bracket notation
console.log(myLaptop['name']);

// Array of Objects
let array = [laptop1, myLaptop];
console.log(array)

console.log(array[0].name); //Lenovo

// Initializing/Adding/Deleting/Reassining Object Properties

let car = {};

console.log(car);

// Initializing/adding object properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties');
console.log(car);

// adding object properties using bracket notation
car['manufacture date'] = 2019;
console.log(car);
console.log(car['manufacture date']);

// Deleting object properties
delete car['manufacture date']
console.log('Result from deleting properties');
console.log(car);

// Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log('reassining properties')
console.log(car);

// Object Methods
// a mehod is a function which is a property of an object

let person = {
	name: 'John',
	talk: function (){
		console.log(`Hello my name is ${this.name}`)
	}
}

person.talk();
console.log(person);

person.walk = function(){
	console.log(this.name+ ' walked 25 steps forward');
}

person.walk();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}.
			I live at ${this.address.city}, ${this.address.country}`);
	}
}

friend.introduce();

// scenario
// 1. We would like to create a game that would have several pokemon interact with each other
// Every pokemon would have the same set of stats, properties and functions

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This Pokemon tackled targetPokemon');
		console.log("targetPokemon's health is now reduce to_targetPokemonhealth_")
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}


// creating an object constructor instead of object literals

function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	// methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		console.log("targetPokemon's health is now reduced.")
	};
	this.faint = function(){
		console.log(`${this.name} fainted.`)
	}
}

// Create new instances
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);

rattata.faint();